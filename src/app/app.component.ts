import { Component, OnInit } from '@angular/core';
import { SuggestionService } from './suggestion.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent implements OnInit{
  public motReference: string = "gros";
  public listeMots: string[] = ["gros", "gras", "graisse", "agressif", "go", "ros", "gro"];
  public numberOfSuggestions = 2;
  results:string[]=[];
  constructor(private suggestionService:SuggestionService){
  }
  ngOnInit(): void {
    const suggestions = this.suggestionService.getSuggestions(this.motReference,this.listeMots,this.numberOfSuggestions);
    this.results = suggestions.map(e => e.mot);
  }
}
