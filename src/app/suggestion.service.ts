import { Injectable } from '@angular/core';
import { Suggestion } from './suggestion';
@Injectable({
    providedIn: 'root'
})
export class SuggestionService {
    /**
     * Suggestions
     * @param motReference 
     * @param listeMots 
     * @param nbTerme 
     * @returns liste de Suggestion
     */
    public getSuggestions(terme: string, choices: string[], numberOfSuggestions: number): Suggestion[] {
        let suggestions: Suggestion[] = [];
        for (let mot of choices) {
            let difference = 0;            
            if (mot.length >= terme.length) {               
                if (!mot.startsWith(terme.charAt(0))) {
                    let motSub= this.trouverMotAPartirPosition(mot,terme);
                    difference= motSub ? this.comparerMots(0, terme,motSub) : 0;
                } else {
                    difference = this.comparerMots(0, terme, mot);
                }
                suggestions.push(new Suggestion(mot, mot.length, difference));
                console.log(`${mot} = ${difference} différences `);
            }else{
                console.log(`${mot} = pas du tout similaire (pas assez de lettres)`);
            }                      
        }
        suggestions.sort((a, b) => {
            if (a.longueur !== b.longueur) {
                return a.longueur - b.longueur;
            } else {
                return a.difference - b.difference;
            }
        });
        return suggestions.slice(0, numberOfSuggestions);
    }
    /**
     * 
     * @param index 
     * @param terme 
     * @param mot 
     * @returns 
     */
    private comparerMots(index: number, terme: string, mot: string): number {
        let nbreDifference = 0;
        for (let i = index; i < terme.length; i++) {
            if (terme.charAt(i) === mot.charAt(i)) {
                this.comparerMots(i + 1, terme.substring(i + 1, terme.length), mot.substring(i + 1, terme.length));
            } else {
                nbreDifference += 1;
            }
        }
        return nbreDifference;
    }
    /**
     * 
     * @param mot 
     * @param terme 
     * @returns 
     */
    trouverMotAPartirPosition (mot:string,terme:string){
        return mot.indexOf(terme.charAt(0)) != -1 ? mot.substring(mot.indexOf(terme.charAt(0))) : null;
    }
}