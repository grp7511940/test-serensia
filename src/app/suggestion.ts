export class Suggestion {
  mot:string;
  longueur: number;
  difference: number;
    constructor(private _mot: string, public _longueur: number,_difference:number) {
        this.mot = _mot;
        this.longueur = _longueur;
        this.difference = _difference;
    }
}
